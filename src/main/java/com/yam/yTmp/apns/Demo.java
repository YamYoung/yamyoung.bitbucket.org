package com.yam.yTmp.apns;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;

/**
 * @author yam<toyangmin@gmail.com>
 *         datetime: 2015/3/27 15:47
 */
public class Demo {
    public static void main(String[] args) {
        ApnsService apns
                = APNS.newService()
                .withCert("path/2/p12", "passwd")
                .asPool(10)
                .withSandboxDestination().build();


        String payload = APNS.newPayload().alertBody("hello").build();

        apns.push("token", payload);
    }
}
