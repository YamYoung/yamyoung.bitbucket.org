package com.yam.yTmp.desginPattern.decorator.simpleFilter;

import java.text.SimpleDateFormat;

/**
 * create time: 2015/3/23 17:06
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public abstract class AbstractSimpleFilter implements SimpleFilter {
    private SimpleFilter source = null;

    public AbstractSimpleFilter() {}

    AbstractSimpleFilter(SimpleFilter simpleFilter) {
        this.source = simpleFilter;
    }

    @Override
    public String doFilter(String in) {
        if (source == null) {
            return filter(in);
        } else {
            return filter(source.doFilter(in));
        }
    }

    public abstract String filter(String in);
}
