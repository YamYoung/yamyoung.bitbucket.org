package com.yam.yTmp.desginPattern.decorator.simpleFilter;

/**
 * create time: 2015/3/23 17:08
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class AddPlusLastFilter extends AbstractSimpleFilter {
    public AddPlusLastFilter() { super(); }

    public AddPlusLastFilter(SimpleFilter simpleFilter) {
        super(simpleFilter);
    }

    @Override
    public String filter(String in) {
        return in + "+";
    }
}
