package com.yam.yTmp.desginPattern.decorator.simpleFilter;

/**
 * create time: 2015/3/23 17:13
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class AddPlusFirstFiler extends AbstractSimpleFilter{
    public AddPlusFirstFiler() { super(); }

    public AddPlusFirstFiler(SimpleFilter simpleFilter) {
        super(simpleFilter);
    }

    @Override
    public String filter(String in) {
        return "+" + in;
    }
}
