package com.yam.yTmp.desginPattern.decorator.simpleFilter;

/**
 * create time: 2015/3/23 17:15
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class Demo {

    /*
    这里提供了一个链式的调用。起作用的就是抽象类里面的构造函数。

    装饰器模式的类结构特征是：
    一个接口，定义了向外暴露的操作
    一个抽象类，是上面接口的实现，具有接收接口类型的构造函数
    一堆抽象类的子类， 实际的操作的实现。

    这里提供了一种动态组合的能力。这里只能得到一个线性的调用链，如果希望能像四则运算那样进行组合，
    可以在最后实现的子类里，提供一个用来粘合的类，这个类接受两个接口类型，和一个操作符。
     */
    public static void main(String[] args) {
        SimpleFilter filter =
                new AddPlusFirstFiler(
                        new AddStarLastFilter(
                                new AddPlusFirstFiler(
                                        new AddPlusLastFilter()
                                )
                        )
                );

        System.out.println(filter.doFilter("hello"));
    }
}
