package com.yam.yTmp.desginPattern.decorator.simpleFilter;

/**
 * create time: 2015/3/23 17:14
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class AddStarLastFilter extends AbstractSimpleFilter {
    public AddStarLastFilter(SimpleFilter simpleFilter) {
        super(simpleFilter);
    }

    @Override
    public String filter(String in) {
        return in + "*";
    }
}
