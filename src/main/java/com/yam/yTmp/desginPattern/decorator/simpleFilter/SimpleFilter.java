package com.yam.yTmp.desginPattern.decorator.simpleFilter;

/**
 * create time: 2015/3/23 17:03
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public interface SimpleFilter {
    String doFilter(String in);
}
