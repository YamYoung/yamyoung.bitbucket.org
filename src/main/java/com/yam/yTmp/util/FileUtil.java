package com.yam.yTmp.util;

/**
 * create time: 2015/3/13 16:02
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class FileUtil {
    private static ClassLoader crtClzLoader = Thread.currentThread().getContextClassLoader();

    public static String getResourcesFilePath(String name) {
        return crtClzLoader.getResource("").getFile()  + name;
    }
}
