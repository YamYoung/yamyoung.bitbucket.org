package com.yam.yTmp.freemarker;

import java.util.List;

/**
 * create time: 2015/4/3 17:21
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class BeanWIthCustomWrapper {
    private List<String> list1;
    private List<String> list2; // 只想这个字段被解析到

    public BeanWIthCustomWrapper(List<String> list1, List<String> list2) {
        this.list1 = list1;
        this.list2 = list2;
    }

    public List<String> getList1() {
        return list1;
    }

    public void setList1(List<String> list1) {
        this.list1 = list1;
    }

    public List<String> getList2() {
        return list2;
    }

    public void setList2(List<String> list2) {
        this.list2 = list2;
    }
}
