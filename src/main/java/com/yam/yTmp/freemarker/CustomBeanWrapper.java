package com.yam.yTmp.freemarker;

import freemarker.template.*;

/**
 * create time: 2015/4/3 17:23
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class CustomBeanWrapper extends DefaultObjectWrapper {
    @Override
    protected TemplateModel handleUnknownType(Object obj) throws TemplateModelException {
        if (obj instanceof BeanWIthCustomWrapper) { // 自定义处理方式
            BeanWIthCustomWrapper bean = (BeanWIthCustomWrapper) obj;
            return new SimpleSequence(bean.getList2(), this);
        }

        return super.handleUnknownType(obj);
    }
}
