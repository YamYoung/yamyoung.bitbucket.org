package com.yam.yTmp.freemarker;

import com.yam.yTmp.util.FileUtil;
import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.template.*;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;

/**
 * Datetime: 2015/4/2 0:35
 *
 * @author yam<toyangmin@gmail.com>
 */
public class BasicDataBindDemo {

    public static void main(String[] args) throws IOException, TemplateException {
        Map root = new HashMap(); // 数据模型

        Configuration cfg = new Configuration();
        String templateDirPath = FileUtil.getResourcesFilePath("freemarker/templates");
        cfg.setDirectoryForTemplateLoading(new File(templateDirPath));
        // 模板如何检索数据
        cfg.setObjectWrapper(new DefaultObjectWrapper());

        // 默认使用 [#ftl] 形式的语法
        // cfg.setTagSyntax(Configuration.SQUARE_BRACKET_TAG_SYNTAX);

        //自动检测语法形式，遇到的第一个标签是尖括号就用尖括号，方括号就用方括号
        cfg.setTagSyntax(Configuration.AUTO_DETECT_TAG_SYNTAX);

        // default object wrapper
        //cfg.setObjectWrapper(new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_22).build());

        // custom object wrapper
        cfg.setObjectWrapper(new CustomBeanWrapper());
        BeanWIthCustomWrapper bwc =
                new BeanWIthCustomWrapper(
                        Arrays.asList("hello", "kitty"),
                        Arrays.asList("hello", "dog")
                );
        root.put("ooo", bwc);


        // data model
        // 支持的数据类型
        // - 标量     String,Number,Boolean,Date
        // - 容器     List,Hash,Set
        // - 子程序   Method&Function ($), User defined cmd(@)
        // - 其他     Node
        // string
        root.put("user", "yam");
        // int
        root.put("n", 100);
        // boolean
        root.put("isOk", false);
        // date
        root.put("now", new Date());

        // list
        List<String> list = new ArrayList<>();
        list.add("yam");
        list.add("kitty");
        list.add("dog");
        root.put("list", list);

        // arbitrary Object
        SubEntity a = new SubEntity("a", "I am a");
        SubEntity b = new SubEntity("b", "I am b");
        SubEntity c = new SubEntity("c", "I am c");
        SubEntity d = new SubEntity("d", "I am d");

        Entity e = new Entity();
        e.setSubEntities(Arrays.asList(a, b));
        Map<String, SubEntity> m = new HashMap<>();
        m.put("c", c);
        m.put("d", d);
        e.setSubEntityMap(m);
        root.put("custom", e);

        // freemarker template
        Template template = cfg.getTemplate("basic.fml");

        // binding data and template
        Writer out = new OutputStreamWriter(System.out);
        template.process(root, out);
        out.flush();

    }
}
