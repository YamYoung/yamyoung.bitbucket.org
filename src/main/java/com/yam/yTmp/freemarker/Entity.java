package com.yam.yTmp.freemarker;

import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * create time: 2015/4/3 16:26
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class Entity {
    private List<SubEntity> subEntities;
    private Map<String, SubEntity> subEntityMap;


    public List<SubEntity> getSubEntities() {
        return subEntities;
    }

    public void setSubEntities(List<SubEntity> subEntities) {
        this.subEntities = subEntities;
    }

    public Map<String, SubEntity> getSubEntityMap() {
        return subEntityMap;
    }

    public void setSubEntityMap(Map<String, SubEntity> subEntityMap) {
        this.subEntityMap = subEntityMap;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this, true);
    }
}
