package com.yam.yTmp.freemarker;

/**
 * create time: 2015/4/3 16:27
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class SubEntity {
    private String name;
    private String Description;

    public SubEntity(String name, String description) {
        this.name = name;
        Description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
