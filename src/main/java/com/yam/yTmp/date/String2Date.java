package com.yam.yTmp.date;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * create time: 2015/3/20 16:23
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class String2Date {
    private SimpleDateFormat dateTimeFormat
            = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
    private Pattern dateTimeMatcher = Pattern.compile(
            "^(19|20)\\d\\d-(0[1-9]|1[012])-(0[1-9]|[1-2][0-9]|3[0-9])\\s([01][0-9]|2[0-4]):([0-5][0-9]|60):([0-5][0-9]|60)$");

    private SimpleDateFormat dateOnlyFormat
            = new SimpleDateFormat("yyyy-MM-dd");
    private Pattern dateOnlyMatcher = Pattern.compile(
            "^(19|20)\\d\\d-(0[1-9]|1[012])-(0[1-9]|[1-2][0-9]|3[0-9])$");

    private Pattern unixTimestamp = Pattern.compile("^14[0-9]{11}");

    public Date convert(String source) {
        Date date = null;
        source = source.trim();

        Matcher matcher = dateTimeMatcher.matcher(source);
        try {
            while (true) {
                // yyyy-MM-dd
                if (matcher.matches()) {
                    date = dateTimeFormat.parse(source);
                    break;
                }
                // yyyy-MM-dd HH-mm-ss
                matcher = dateOnlyMatcher.matcher(source);
                if (matcher.matches()) {
                    date = dateOnlyFormat.parse(source);
                    break;
                }
                // unix timestamp: 1426839737343
                matcher = unixTimestamp.matcher(source);
                if (matcher.matches()) {
                    date = new Date(Long.parseLong(source));
                    break;
                }

                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }
}
