package com.yam.yTmp.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * create time: 2015/3/20 15:36
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class DateFormatterDemo {
    public static void main(String[] args) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        Date now = new Date();

        String dateStr = format.format(now);

        System.out.println(dateStr);

        try {
            Date copyNow = format.parse(dateStr);
            System.out.println(copyNow);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
