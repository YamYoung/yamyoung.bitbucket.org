package com.yam.yTmp.nio.ibmDemo;// $Id$

import java.nio.*;

public class UseFloatBuffer
{
  static public void main( String args[] ) throws Exception {
    FloatBuffer buffer = FloatBuffer.allocate( 10 );

    for (int i=0; i<buffer.capacity(); ++i) {
      float f = (float)Math.sin( (((float)i)/10)*(2*Math.PI) );
      buffer.put( f ); // add element to buffer
    }

    buffer.flip();

    while (buffer.hasRemaining()) { // check the remind size of elements
      float f = buffer.get(); // get element from buffer
      System.out.println( f );
    }
  }
}
