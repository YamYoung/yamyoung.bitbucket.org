package com.yam.yTmp.nio;

import com.yam.yTmp.util.FileUtil;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * create time: 2015/3/16 9:38
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class SimpleUsage {
    private static RandomAccessFile file;
    private static String filePath;

    static {
        filePath = FileUtil.getResourcesFilePath("lucene/data.txt");
        System.out.println(filePath);

        try {
            file = new RandomAccessFile( filePath, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static void simpleRead() {
        ByteBuffer buffer = ByteBuffer.allocate(64);

        Charset charset = Charset.forName("UTF8");
        //buffer.order(ByteOrder.LITTLE_ENDIAN);
        //System.out.println(buffer.order());
        System.out.println(buffer.isDirect());

        CharBuffer charBuffer = null;

        //CharBuffer charBuffer = charset.decode(buffer);
        //CharBuffer charBuffer = buffer.asCharBuffer();

        FileChannel channel = file.getChannel();
        try {
            int length = channel.read(buffer);

            long start = System.currentTimeMillis();
            byte[] buf = new byte[64];
            while (length != -1) {
                buffer.flip();
                // 解码 byteBuffer 为 charBuff
                charBuffer = charset.decode(buffer);
                while (charBuffer.hasRemaining()) {
                    //char c = Character.valueOf((char) buffer.get());
                    char c = charBuffer.get();//buffer.getChar();
                    System.out.print(c);
                }

                //System.out.print(charset.decode(buffer));

                // 这里比上面的 while 块的做法快了一倍，一次读出全部内容
                //buffer.get(buf, 0, buffer.remaining());

                //charBuffer.clear();
                buffer.clear();
                length = channel.read(buffer);
            }
            System.out.println("nio read file: " + (System.currentTimeMillis() - start));
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void streamRead() {
        try {
            BufferedInputStream inputStream = new BufferedInputStream(
                    new FileInputStream(new File(filePath))
            );

            byte[] buff = new byte[64];

            long start = System.currentTimeMillis();
            while (inputStream.read(buff) != -1) {
                for (byte b: buff) {
                    char c = (char) b;
                    //System.out.print(c);
                }
            }
            System.out.println("stream read file: " + (System.currentTimeMillis() - start));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void transferFile() throws IOException {
        FileChannel fromChanel = file.getChannel();
        long length = fromChanel.size();

        FileChannel toChannel
                = new RandomAccessFile(new File("dump/copy.txt"), "rw").getChannel();


        toChannel.transferFrom(fromChanel, 0L, length);
    }

    public static void main(String[] args) {
        simpleRead();
        streamRead();
    }
}
