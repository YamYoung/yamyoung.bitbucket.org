package com.yam.yTmp.testAssert;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

/**
 * create time: 2015/3/17 16:22
 *
 * @author : yam <yangm@corp.21cn.com>
 */
public class IsStartWith extends BaseMatcher<String>{
    private String prefix;

    public IsStartWith(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public boolean matches(Object item) {
        if (!(item instanceof String)) return false;

        return ((String) item).startsWith(prefix);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("String start with " + prefix);
    }
}
