package com.yam.yTmp.util;

import com.yam.yTmp.testAssert.IsStartWith;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;
import java.util.Collection;


import static org.junit.Assert.*;
import static org.junit.runners.Parameterized.*;

// 此注释表示单元测试接受 @Parameters 注解的方法提供的参数，并分别对其执行测试
@RunWith(Parameterized.class)
public class FileUtilTest {

    // 每个测试方法执行前都会执行这个方法
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    // 此注解提供的参数列表，会被相应的构造函数读取，以初始化测试对象
    @Parameters
    public static Collection filePathParam() {
        return Arrays.asList(new Object[]{
                "/",
                "/lucene",
                "/anything"
        });
    }

    private String CompilePath = "/E:/IDEA_workspace/yTmp/target/test-classes/";
    private String inPath;
    public FileUtilTest(String inPath) {
        this.inPath = inPath;
    }

    @Test
    public void testGetResourcesFilePath() throws Exception {
        assertEquals(CompilePath + inPath, FileUtil.getResourcesFilePath(inPath));
    }

    @Test
    public void testCatchException() {
        try {
            int i = 1/0;
            fail();
        } catch (Exception e) {
            assertEquals("/ by zero", e.getMessage());
        }
    }

    private Matcher isStartWith(String prefix){
        return new IsStartWith(prefix);
    }

    @Test
    public void testThatDemo() {
        assertThat(inPath, isStartWith("/"));
    }
}